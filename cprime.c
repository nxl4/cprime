/***********************************************************
 *   ___ ___     _                                         *
 *  / __| _ \_ _(_)_ __  ___                               *
 * | (__|  _/ '_| | '  \/ -_)                              *
 *  \___|_| |_| |_|_|_|_\___|                              *
 *                                                         *
 * Program:                                                *
 *     CPrime: Arbitrarily Large Prime Number Tester       *
 *                                                         *
 * Date:                                                   *
 *     21 December 2019                                    *
 *                                                         *
 * Version:                                                *
 *     0.1                                                 *
 *                                                         *
 * Author:                                                 *
 *     nxl4@protonmail.com                                 *
 *                                                         *
 * Source:                                                 *
 *     https://gitlab.com/nxl4/cprime                      *
 *                                                         *
 * License:                                                *
 *     GPL v3.0                                            *
 *                                                         *
 * Compiles With:                                          *
 *     gcc -o cprime cprime.c -lgmp                        *
 *                                                         *
 * Purpose:                                                * 
 *     This program is designed to test arbitrarily large  *
 *     numbers, to determine if they are or are not prime. *
 *     The program is argument-based. It has one optional  *
 *     argument, -h/--help, that is used to print the help *
 *     menu. The sole positional argument is the integer   *
 *     value to be tested. When a value is input, the pro- *
 *     gram will print the following:                      *
 *                                                         *
 *         1. The input value passed as the argument       *
 *         2. Whether or not the value is a valid integer  *
 *             2a. The first divisor (if any)              *
 *             2b. Whether or not the number is prime      *
 *                                                         *
 * Usage:                                                  *
 *     cprime [OPTION]|OBJECT                              *
 *                                                         *
 * Options:                                                *
 *     -h, --help  shows help message                      *
 *                                                         *
 * Examples:                                               *
 *     $ cprime 23                                         *
 *     [*] 23                                              *
 *     [+] It's a valid integer!                           *
 *     [+] It's prime!                                     *
 *                                                         *
 *     $ cprime 24                                         *
 *     [*] 24                                              *
 *     [+] It's a valid integer!                           *
 *     [-] It's divisible by 2!                            *
 *     [-] It's not prime!                                 *
 *                                                         *
 ***********************************************************/

#include <assert.h>
#include <ctype.h>
#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void title(void) {
    /* This function prints the title splash screen. */
    printf("   ___ ___     _           \n");
    printf("  / __| _ \\_ _(_)_ __  ___ \n");
    printf(" | (__|  _/ '_| | '  \\/ -_)\n");
    printf("  \\___|_| |_| |_|_|_|_\\___|\n");
    printf("                           \n");
    printf("CPrime, 0.1 (https://gitlab.com/nxl4/cprime): ");
    printf("Arbitrarily Large Prime Number Tester\n\n");
}

void usage(char *program_name) {
    /* This function prints the usage instructions and
     * lists optional arguments. */
    printf("Usage is %s [OPTION]|OBJECT\n\n", program_name);
    printf("Options:\n");
    printf("  -h, --help      shows help message\n\n");
}

void examples(char *program_name) {
    /* This function prints examples of successful and
     * failed usages. */
    printf("Examples:\n");
    printf("  $ %s 23\n", program_name);
    printf("  [*] 23\n");
    printf("  [+] It's a valid integer!\n");
    printf("  [+] It's prime!\n\n"); 
    printf("  $ %s 24\n", program_name);
    printf("  [*] 24\n");
    printf("  [+] It's a valid integer!\n");
    printf("  [-] It's divisible by 2!\n");
    printf("  [-] It's not prime!\n\n");
}

void input_error_1(char *program_name) {
    /* This function handles instances where no argument is given
     * after the program is invoked. It prints the usage block,
     * then follows this with an error message, and exits. */
    usage(program_name);
    printf(
        "%s: error: the following arguments are required:\n", 
        program_name
    );
    printf("  [-h]|OBJECT\n");
    exit(1);
}

void input_error_2(char *program_name, char *arg_one) {
    /* This function handles instances where an invalid option
     * is passed as an argument. It prints the usage block, then
     * follows this with an error message identifying the faulty
     * argument, and then exits. */
    usage(program_name);
    printf(
        "%s: error: unrecognized arguments: %s\n", 
        program_name, 
        arg_one
    );
    exit(2);
}

void input_error_3(char *program_name) {
    /* This function handles instances where an invalid number of
     * arguments (i.e. more than 1) are passed. It prints the 
     * usage block, follows this with an error message, and then
     * exits. */
    usage(program_name);
    printf(
        "%s: error: too many arguments\n",
        program_name
    );
    exit(3);
}

void input_error_4(void) {
    /* This function handles instances where an invalid value is
     * passed as an integer argument. It prints an error message,
     * and then exits. */
    printf("[-] It's not a valid integer!\n");
    exit(4);
}

void help_msg(char *program_name) {
    /* This function prints the HELP message, based on the -h and
     * --help arguments being passed. The help messages is composed
     * of the title, usage, and example functions' contents. After
     * printing its contents, this function exits the program. */
    title();
    usage(program_name);
    examples(program_name);
    exit(0);
}

int prime_test(mpz_t n) {
    /* ************************************************************
     * This function accepts a GMP integer as input, and runs it  *
     * through an algorithm to determine if it is prime. For time *
     * optimization, the algorithm takes the following steps to   *
     * calculate whether the input value is prime:                *
     *                                                            *
     *     1. Is the number 0? NOT PRIME.                         *
     *     2. Is the number negative? NOT PRIME.                  *
     *     3. Is the number 1? NOT PRIME.                         *
     *     4. Is the number 2? PRIME.                             *
     *     5. Is the number even? NOT PRIME.                      *
     *     6. Is the number odd?                                  *
     *         6a. Is the number divisible by anything less than  *
     *             half the value of itself? NOT PRIME.           *
     *         6b. If not? PRIME.                                 *
     *                                                            *
     * Once the algorithm completes its calculation, the function *
     * either returns 0 if the number is not prime, or 1 if the   *
     * number is prime.                                           *
     **************************************************************/

    mpz_t i; /* declare GMP iterator */
    mpz_t h; /* declare GMP half value */

    mpz_init(i);      /* initialize iterator */
    mpz_set_ui(i, 3); /* set iterator count to 3 */

    mpz_init(h);      /* initialize the half */
    mpz_set_ui(h, 0); /* set half value to 0 */

    /* tests if value equals zero */
    if (mpz_sgn(n) == 0) {
        printf("[-] It's zero!\n");
        return (0);
    /* test if value is negative */
    } else if (mpz_sgn(n) == -1) {
        printf("[-] It's negative!\n");
        return (0);
    /* tests if value is 1 */
    } else if (mpz_cmp_si(n, 1) == 0) {
        printf("[-] It's one!\n");
        return (0);
    /* tests if value is 2 */
    } else if (mpz_cmp_si(n, 2) == 0) {
        printf("[+] It's two!\n");
        return (1);
    /* test if value is even */
    } else if (mpz_even_p (n) == 1) {
        printf("[-] It's divisible by 2!\n");
        return (0);
    /* tests if value is odd */
    } else if (mpz_even_p (n) == 0) {
        mpz_cdiv_q_ui(h, n, 2);
        /* loops while i is less than n/2 */
        while (mpz_cmp(h, i) > 0) {
            /* identifies non-primes and divisors */
            if (mpz_divisible_p(n, i) == 1) {
                printf("[-] It's divisible by ");
                mpz_out_str(stdout, 10, i);
                printf("!\n");
                return (0);
                break;
            }
            /* increases counter value by 1 */
            mpz_add_ui(i, i, 1);
        }
        return (1);
    }
}

int prime_integer(char *c) {
    /* This function casts the validated input string as a GMP
     * integer. The new GMP integer value is subjected to the 
     * prime number testing function. Based on the results of the
     * test, a message is printed. Then, the function exits. */

    mpz_t n;      /* declares GMP number */
    int flag;     /* declares flag for base 10 parsing*/
    int is_prime; /* declares prime test results */

    mpz_init(n);      /* initializes GMP number */
    mpz_set_ui(n, 0); /* sets initial value to 0 */

    /* parses the input string as a base 10 number 
     * if parsed input is not 0, then function fails */
    flag = mpz_set_str(n, c, 10);
    assert (flag == 0);

    /* runs prime test on GMP number */
    is_prime = prime_test(n);

    /* handles results of prime test */
    if (is_prime == 0) {
        printf("[-] It's not prime!\n");
    } else if (is_prime == 1) {
        printf("[+] It's prime!\n");
    }
    return (0);
    exit(0);
}

int input_validator(char *c) {
    /* This function validates input by ensuring that each 
     * character constituting the string array is an integer.
     * It returns 1 if the input is invalid, and 0 if the input
     * is valid. */

    int i = 0; /* declare and initialize iterator */

    /* iterate through each member of array */
    while(c[i]) {
        /* test if element is a non-integer */
        if (!isdigit(c[i])) {
            /* abend and exit function */
            return(1);
            break;
        }
        i++; /* increase iterator */
    }
    return(0);
}

int integer_handler(char *c) {
    /* This function handles supposed integers passed as arguments
     * into the program. It begins by testing the input, to determine
     * if each character of the string array is an integer. Invalid
     * strings result in an error function being called. Valid strings
     * result in a confirmation message being printed, and the string
     * value being passed to the first prime testing function. */

    /* declare variable for input validator, initialize with input
     * validation function results for character array */
    int is_valid = input_validator(c);

    /* handles valid integer strings */
    if (is_valid == 1) {
        input_error_4();
    /* handles invalid integer strings */
    } else if (is_valid == 0) {
        printf("[+] It's a valid integer!\n");
        prime_integer(c);
    }
    return(0);
}

int main(int argc, char *argv[]) {
    /* This function parses the command line arguments passed to
     * the program. For valid arguments, it either calls the help
     * function or initiates the prime test as appropriate. For
     * invalid arguments, the appropriate error functions are 
     * called instead. */

    char *program_name; /* declares program name var */
    char *arg_one;      /* declares first arg var */
    char *arg_two;      /* declares second arg var */

    /* initializes program name with first argument */
    program_name = argv[0];

    /* handles situations where no arguments are passed */
    if (argc == 1) {
        input_error_1(program_name);
    /* handles situations where one argument is passed */
    } else if (argc == 2) {
        arg_one = argv[1];
        /* tests if first arg is help */
        if (
            (strcmp(argv[1], "-h") == 0) || 
            (strcmp(argv[1], "--help") == 0)
        ) {
            help_msg(program_name);
        /* calls error function for invalid args */
        } else if (argv[1][0] == '-') {
            input_error_2(program_name, arg_one);
        /* passes possible integer args to validator */
        } else {
            printf("[*] %s\n", arg_one);
            integer_handler(arg_one);
        }
    /* handles situations where more than one argument is passed */
    } else {
        /* tests if first arg is help */
        if (
            (strcmp(argv[1], "-h") == 0) || 
            (strcmp(argv[1], "--help") == 0)
        ) {
            help_msg(program_name);
        /* calls error function */
        } else {
            input_error_3(program_name);
        }
    }
    return(0);
}
