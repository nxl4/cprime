CC = gcc
FILES = cprime.c
LIBS = lgmp
OUT_EXE = cprime
PREFIX ?= /usr

all:
	@echo Run \'make build\' to complie CPrime.
	@echo Run \'make clean\' to remove previously comiled build of CPrime.
	@echo Run \'make install\' to install CPrime.
	@echo Run \'make uninstall\' to uninstall CPrime.

build: $(FILES)
	$(CC) -o $(OUT_EXE) $(FILES) -$(LIBS)

clean:
	rm -f *.o core
	rm -f $(OUT_EXE)

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p cprime $(DESTDIR)$(PREFIX)/bin/cprime
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/cprime

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/cprime
