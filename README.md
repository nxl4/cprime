```
   ___ ___     _ 
  / __| _ \_ _(_)_ __  ___ 
 | (__|  _/ '_| | '  \/ -_)
  \___|_| |_| |_|_|_|_\___|
                           
CPrime, version 0.1
```

## General Description

CPrime a command line utility that is designed to test arbitrarily large number, to determine if they are or are not prime. The ability to handle extremely large numbers is done via the C `gmp.h` library. 

## Input

Data is input into the utility via command line arguments. There are two categories of arguments accepted: (1) one positional argument, and (1) optional argument.

### Positional Argument

The single positional defines the integer value to be tested by the utility's algorithm.

### Optional Argument

The single optional argument available is:

* `-h, --help` Shows Help Message and Exits

The positional and optional arguments are mutually exclusive. Although, as long as the `-h` argument is the first one passed, it will function properly even if additional illegal arguments are passed after it.

## Usage

The general usage structure of the utility is:

```shell
cprime [-h]|[OBJECT]
```

## Algorithm and Output

The output is simple and relatively straight forward. When a potentially valid positional argument is passed, the utility will print that with a `[*]` prefix. Next, it will validate the input, by checking that each character in the string array is an integer. The results of this validation will then be printed to the CLI, preceded by either a `[-]` or `[+]` prefix, respectively for validation failures and successes. In the case of validation failures, the program exits. In the case of successes, the program will then convert the integer string into a `gmp` integer, and subject it to the prime test algorithm. The algorithm will first test for the following non-prime conditions:

* The number is 0.
* The number is 1.
* The number is negative.
* The number is even.

If any of these are the case, the program will print a message that the number *is not* prime, and will state the reason (both messages prefixed with `[-]` indicators). In the special case that the number is 2, the program will print a message that the number *is* prime (prefixed with an `[+]` indicator). For all other odd numbers (`n`) , the algotithm will iterate through each value from 3 to `n / 2`, to determine if has any divisors. If a divisor is found, the loop breaks, and the divisor is printed with a message that the number is not prime (both prefixed by `[-]` indicators). If no divisors are found, the number is deemed prime, and the message is printed to the console.

For an example of a prime number being tested, see:

![out_1](img/prime.png)

For an example of a non-prime number being tested, see:

![out_2](img/not_prime.png)

## Installation

### UNIX-like

The easiest way to install this utility is to first clone the repository:

```shell
git clone https://gitlab.com/nxl4/cprime.git
```

Then, run the `make` command on the `makefile` from within the cloned repository's new directory to *first* compile the program:

```shell
make clean build
```

And once the program has been compiled, it can be installed for general use throughout the system:

```shell
sudo make install
```

Likewise, the `make` command can be used to uninstall the utility:

```shell
sudo make uninstall
```

### Windows

**TODO:** *Develop Windows-specific installation script.*

This utility has not been tested in *any* Windows environments.

## Issues

Please report any issues to the [Issue Tracker](https://gitlab.com/nxl4/cprime/issues/new) for this repository. 

## Author

nxl4: [nxl4@protonmail.com](nxl4@protonmail.com)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
